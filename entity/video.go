package entity

import (
	"time"
)

type Person struct {
	ID        uint64 `json:"id" gorm:"primary_key;auto_increment"`
	FirstName string `json:"firstName" gorm:"type:varchar(32)" binding:"required"`
	LastName  string `json:"lastName" gorm:"type:varchar(32)" binding:"required"`
	Age       uint8  `json:"age" binding:"gte=1,lte=130"`
	Email     string `json:"email" gorm:"type:varchar(256)" binding:"required,email"`
}

type Video struct {
	ID          uint64    `json:"id" gorm:"primary_key;auto_increment"`
	Title       string    `json:"title" gorm:"type:varchar(100)" binding:"min=2,max=10" validate:"is-cool"`
	Description string    `json:"description"gorm:"type:varchar(200)" binding:"max=20"`
	URL         string    `json:"url" gorm:"type:varchar(256);UNIQUE" binding:"required,url"`
	Author      Person    `json:"author" gorm:"foreignkey:PersonID" binding:"required"`
	PersonID    uint64    `json:"-"`
	CreatedAt   time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt   time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP"`
}
